FROM ubuntu:16.04

WORKDIR /app

COPY . /app

RUN apt-get update

RUN apt-get install -y software-properties-common

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update
RUN apt-get install -y python3.7

RUN apt-get install -y dkms
RUN apt-get install -y fxload

RUN dpkg -i tmsi-dkms*.deb
RUN apt-get install -y python3-pip

RUN pip3 install --upgrade pip

RUN pip3 install wheel
RUN pip3 install pylsl-1.10.6.zip
RUN pip3 install obci_utils-2.7.0+6.g87e3397-py3-none-any.whl
RUN pip3 install obci-2.7.0+6.g87e3397-py2.py3-none-any.whl

RUN pip3 install obci_cpp_amplifiers-2.6.0.post14+g0e3d385-cp35-cp35m-linux_x86_64.whl

RUN apt-get install -y libbluetooth-dev
RUN apt-get install -y libftdi-dev
